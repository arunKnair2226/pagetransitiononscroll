
$(document).ready(function(){
    $(this).scrollTop(0);
});

$(".scrollable").on('scroll',function(event){
  // current scroll top value ----
	var currScroll = $(this).scrollTop();
  // fetch the id of active div from DOM ----
  var currentDiv = $(".active").attr("id");
  // fetch the id of div next to active div from DOM ----
  var nextDiv = $("#"+currentDiv).next().attr("id");
  // fetch the id of div previous to active div from DOM ----
  var prevDiv = $("#"+currentDiv).prev().attr("id");
  // Check if scroll reaches bottom of div and next div exists ----
  if($(this).scrollTop() + $(this).innerHeight()>=($(this)[0].scrollHeight) && (($("#"+nextDiv).length) > 0)){
    $("#"+nextDiv).addClass("active");
    $("#"+currentDiv).removeClass("active").addClass("position-after");
    currentDiv = $("#"+nextDiv);
    $(".active .scrollable").scrollTop(16);
  }
  // Check if scroll reached top and prev div exists ----
  if((currScroll == 0) && (($("#"+prevDiv).length) > 0)){
    $("#"+prevDiv).addClass("active").removeClass("position-after");
    $("#"+currentDiv).removeClass("active");
    currentDiv = $("#"+prevDiv);
    $(".active .scrollable").scrollTop(8);
  }
})