## Requirement

Latest version of jQuery. I used **jquery-3.3.1.min.js** for this demo work.

## Compatibility

Modern browsers such as Chrome, Firefox, and Safari on both desktop and smartphones have been tested.

## Basic Usage

This demo is made to let you transform your sections/pages of web pages. 

Users can scroll through one complete section/page and upon scrolling the complete section/page, it scrolls up/down to show you next/previous section.

**NOTE:**This will only work for sections/pages having height more than viewport height (100vh units).

* You can download the files from repository 
* Add your page content inside ```<div class="content">```
* Add the mandatory styles, js code given below in explaination along with your own styles and functions.

# How to acheive it

## Creating the structure

The HTML structure is just a nested arrangement of ```<div>``` elements, with a scrollable ```<div>``` inside. Feel free to add your own content inside ```<div class="content">```.

```html

<body>
  <!-- MAIN CONTAINER -->
  <div class="fixed body-wrap">
    <!-- PAGE 1 WRAPPER CONTAINER -->
    <div class="fixed div-position active" id="pg1">
      <!-- SCROLLABLE DIV -->
      <div class="scrollable">
        <!-- CONTENT WRAPPER -->
        <div class="content-wrap dsp-flx pg1">
          <!— CONTENT DIV -->
          <div class="content center-content">
           <!-- your content here -->
          </div>
        </div>
      </div>
    </div>
    <!-- PAGE 2 WRAPPER CONTAINER -->
    <div class="fixed div-position" id="pg2">
      <div class="scrollable">
        <div class="content-wrap dsp-flx pg2">
          <div class="content center-content">
            <!-- WRITE ALL YOUR CONTENTS INSIDE THIS WRAP -->
          </div>
        </div>
      </div>
    </div>
    <!-- PAGE 2 ENDS -->
  </div>
  <!-- MAIN CONTAINER ENDS -->
</body>

```

## Adding style

I simply set a height more than 100vh for each ```.content-wrap``` class so that we get a scrollable content section/page.

I also used some background colors and images to get some visual delight to the layout.

```css

/******** MANDATORY PROPERTIES ************/

html,
body {
  height: 100%;
  overflow: hidden;
}

.fixed {
  width: 100%;
  height: 100%;
  padding-right: 15px;
  overflow: hidden;
  position: absolute;
  z-index: 9;
  transition: all 0.7s;
}

.scrollable {
  position: absolute;
  height: 100%;
  width: 100%;
  overflow-y: auto;
   -webkit-overflow-scrolling: touch;
  transition: all 0.7s;
}

.content-wrap {
  width: 100%;
  min-height: calc(100vh + 20px);
  display: flex;
  align-items: center;
  justify-content: center;
}

.content {
  width: calc(100vw - 48px);
  max-width: 980px;
  padding: 64px 0;
}

.div-position {
  transform: translate3d(0, 100vh, 0);
}

.body-wrap {
  overflow: hidden;
}

.active {
  z-index: 10 !important;
  transform: translate3d(0, 0, 0);
}

.position-after{
  transform: translate3d(0, -100vh, 0);
}

/* MEDIA QUERIES */

@media screen and (max-width: 1024px) {
  .content{
    width: 80vw;
    max-width: 100%;
    position: relative;
  }
}

```


## JS code to achieve the scroll effect

We are checking the scroll position to acheive this effect. 

If scroll is touching the top and there is previous page/section available we are showing previous page with transitions. 

Similarly, if scroll is touching the bottom and there is next page/section available we are showing next page with transition effects.

```JavaScript

$(document).ready(function(){
    $(this).scrollTop(0);
});

$(".scrollable").on('scroll',function(event){
  // current scroll top value ----
 var currScroll = $(this).scrollTop();
 // fetch the id of active div from DOM ----
  var currentDiv = $(".active").attr("id");
  // fetch the id of div next to active div from DOM ----
  var nextDiv = $("#"+currentDiv).next().attr("id");
  // fetch the id of div previous to active div from DOM ----
  var prevDiv = $("#"+currentDiv).prev().attr("id");
  // Check if scroll reaches bottom of div and next div exists ----
  if($(this).scrollTop() + $(this).innerHeight()>=($(this)[0].scrollHeight) && (($("#"+nextDiv).length) > 0)){
    $("#"+nextDiv).addClass("active");
    $("#"+currentDiv).removeClass("active").addClass("position-after");
    currentDiv = $("#"+nextDiv);
    $(".active .scrollable").scrollTop(16);
  }
  // Check if scroll reached top and prev div exists ----
  if((currScroll == 0) && (($("#"+prevDiv).length) > 0)){
    $("#"+prevDiv).addClass("active").removeClass("position-after");
    $("#"+currentDiv).removeClass("active");
    currentDiv = $("#"+prevDiv);
    $(".active .scrollable").scrollTop(8);
  }
})

```

## References / Disclaimer

-I do not own any images used in the demo pages. It was taken from google image search for personal use.

-Lorem Ipsum content was taken from : [https://www.lipsum.com/](https://www.lipsum.com/).

-The logo was created using a template from : [Free online logo maker tool](https://editor.freelogodesign.org/).

-Fonts were taken from Google Fonts which are free to use.